package com.cigniti.salesforce.automation.pages;

import java.util.Hashtable;
import org.openqa.selenium.By;
import com.cigniti.salesforce.automation.actions.ActionEngine;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author E001987
 *
 */
public class OpportunitiesPage extends ActionEngine {
  
  private By linkOpportunitiesTab = By.xpath("//a[@title='Opportunities']");
  private By textPageType = By.xpath("//nav[@aria-label='Breadcrumbs']");
  private By buttonNew = By.xpath("//div[@title='New']");
  private By textHeaderNewOpportunity = By.xpath("//div[@class='actionBody']//h2");
  private By rdoFarmingOpportunity = By.xpath("//span[text()='Farming Opportunity']/parent::div/parent::label/div/input");
  private By buttonNaxt = By.xpath("//div[@class='inlineFooter']//button/span[text()='Next']");
  private By textOpportunityName = By.xpath("//span[text()='Opportunity Name']/parent::label/following-sibling::input");
  private By buttonSave = By.xpath("//button[@title='Save']");
  
  /**
   * @param testData
   * @throws Throwable
   */
  public void createOpportunity(Hashtable<String, String> testData) throws Throwable{
    boolean isOpportunityCreated = false;
    try{
      waitForElementToBeClickable(linkOpportunitiesTab, 10);
      jsClick(linkOpportunitiesTab, "Opportunities Tab");
      verifyText(textPageType, testData.get("Opportunity_Page_Title"));
      click(buttonNew, "New button");
      verifyText(textHeaderNewOpportunity, testData.get("New_Opportunity_Header"));
      click(buttonSave, "Save button");
      isOpportunityCreated = isElementPresent(By.xpath("//div/span[text()='"+testData.get("Opportunity_Name")+"']"));
    }catch(Exception e){
      e.printStackTrace();
    }finally{
      if (isOpportunityCreated) {
        reportLogger.log(LogStatus.PASS, "Opportunity '<b style=\"color:blue;\">"+testData.get("Opportunity_Name")+"</b>' created succesfully ");
      } else {
        reportLogger.log(LogStatus.FAIL, "Failed to create Opportunity "+ reportLogger.addScreenCapture(getScreenshot(webDriver, "Failed to create Opportunity")));
      }
    }
  }
  
}
