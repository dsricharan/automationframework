package com.cigniti.salesforce.automation.pages;

import java.util.Hashtable;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.cigniti.salesforce.automation.actions.ActionEngine;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author E001987
 *
 */
public class AccountsPage extends ActionEngine {
  
  private By linkAccountsTab = By.xpath("//a[@title='Accounts']");
  private By textPageType = By.xpath("//nav[@aria-label='Breadcrumbs']");
  private By textSearchFiled = By.xpath("//input[@name='Account-search-input']");
  private By buttonDelete = By.xpath("//div[@title='Delete']");
  private By buttonDeleteAccount = By.xpath("//button[@title='Delete']");
  private By buttonNew = By.xpath("//div[@title='New']");
  private By textHeaderNewAccount = By.xpath("//div[@class='actionBody']//h2");
  private By textAccountName = By.xpath("//span[text()='Account Name']/parent::label/following-sibling::input");
  private By textAccountNameErrorMessage = By.xpath("//span[text()='Account Name']/ancestor::div/following-sibling::ul/li");
  private By textParentAccount = By.xpath("//span[text()='Parent Account']/parent::label/following-sibling::div//input");
  private By imgParentAccount = By.xpath("//span[text()='Parent Account']/parent::label/following-sibling::div//span/img");
  private By textPhone = By.xpath("//span[text()='Phone']/parent::label/following-sibling::input");
  private By selectStatus = By.xpath("//span[text()='Status']/parent::span/following-sibling::div//a");
  private By selectVerticalGrouping = By.xpath("//span[text()='Vertical Grouping']/parent::span/following-sibling::div//a");
  private By selectVertical = By.xpath("//span[text()='Vertical']/parent::span/following-sibling::div//a");
  private By selectSubVertical1 = By.xpath("//span[text()='Sub Vertical 1']/parent::span/following-sibling::div//a");
  private By textISTRepresentative = By.xpath("//span[text()='IST Representative']/parent::span/following-sibling::div//a");
  private By textRevenue = By.xpath("//span[text()='Revenue']/parent::label/following-sibling::input");
  private By selectBusinessUnit = By.xpath("//span[text()='Business Unit']/parent::span/following-sibling::div//a");
  private By textareaBillingStreet = By.xpath("//span[text()='Billing Street']/parent::label/following-sibling::textarea");
  private By textBillingCity = By.xpath("//span[text()='Billing City']/parent::label/following-sibling::input");
  private By textBillingStateOrProvince = By.xpath("//span[contains(text(),'Billing State')]/parent::label/following-sibling::input");
  private By textBillingZipOrPostalCode = By.xpath("//span[contains(text(),'Billing Zip')]/parent::label/following-sibling::input");
  private By textBillingCountry =  By.xpath("//span[text()='Billing Country']/parent::label/following-sibling::input");
  private By buttonSave = By.xpath("//button[@title='Save']");
  
  /**
   * @param testData
   * @throws Throwable
   */
  public void createAccount(Hashtable<String, String> testData) throws Throwable{
    boolean isAccountCreated = false;
    try{
      waitForElementToBeClickable(linkAccountsTab, 10);
      jsClick(linkAccountsTab, "Accounts Tab");
      verifyText(textPageType, testData.get("Accounts_Page_Title"));
      cleanUpAccount(testData.get("Account_Name"));
      click(buttonNew, "New button");
      waitForElementPresent(textHeaderNewAccount, "Header New Account");
      verifyText(textHeaderNewAccount, testData.get("New_Account_Header"));
      click(buttonSave, "Save button");
      verifyText(textAccountNameErrorMessage, testData.get("Text_Field_Error_Message"));
      type(textAccountName, testData.get("Account_Name"), "Account Name");
      type(textParentAccount, testData.get("Parent_Account"), "Parent Account");
      click(imgParentAccount, testData.get("Parent_Account"));
      type(textPhone, testData.get("Phone").toString(), "Phone");
      click(selectStatus, "Status");
      click(By.xpath("//a[@title='"+testData.get("Status")+"']"), testData.get("Status"));
      click(selectVerticalGrouping, "Vertical Grouping");
      click(By.xpath("//a[@title='"+testData.get("Vertical_Grouping")+"']"), testData.get("Vertical_Grouping"));
      click(selectVertical, "Vertical");
      click(By.xpath("//a[@title='"+testData.get("Vertical")+"']"), testData.get("Vertical"));
      click(selectSubVertical1, "Sub Vertical 1");
      click(By.xpath("//a[@title='"+testData.get("Sub_Vertical_1")+"']"), testData.get("Sub_Vertical_1"));
      click(textISTRepresentative, "IST Representative");
      click(By.xpath("//a[@title='"+testData.get("IST_Representative")+"']"), testData.get("IST_Representative"));
      type(textRevenue, testData.get("Revenue"), "Revenue");
      click(selectBusinessUnit, "Business Unit");
      click(By.xpath("//a[@title='"+testData.get("Business_Unit")+"']"), testData.get("Business_Unit"));
      type(textareaBillingStreet, testData.get("Billing_Street"), "Billing Street");
      type(textBillingCity, testData.get("Billing_City"), "Billing City");
      type(textBillingStateOrProvince, testData.get("Billing_State"), "Billing State/Province");
      type(textBillingZipOrPostalCode, testData.get("Billing_Zip").toString(), "Billing Zip/Postal Code");
      type(textBillingCountry, testData.get("Billing_Country"), "Billing Country");
      click(buttonSave, "Save button");
      isAccountCreated = isElementPresent(By.xpath("//div/span[text()='"+testData.get("Account_Name")+"']"));
    }catch(Exception e){
      e.printStackTrace();
    }finally{
      if (isAccountCreated) {
        reportLogger.log(LogStatus.PASS, "Account '<b style=\"color:blue;\">"+testData.get("Account_Name")+"</b>' created succesfully ");
      } else {
        reportLogger.log(LogStatus.FAIL, "Failed to create Account "+ reportLogger.addScreenCapture(getScreenshot(webDriver, "Failed to create Account")));
      }
    }
  }
  
  private void cleanUpAccount(String accountName) throws Throwable {
    boolean isAccountDeleted = true;
    try {
      click(textSearchFiled, "Search account");
      type(textSearchFiled, accountName, accountName);
      getWebElement(textSearchFiled).sendKeys(Keys.ENTER);
      By account = By.xpath("//a[text()='" + accountName + "']");
      if (getWebElements(account).size() > 0) {
        staleClick(account, accountName);
        click(buttonDelete, "Button Delete");
        click(buttonDeleteAccount, "Button Delete");
        refreshPage();
        waitForPageToLoad();
      }
    } catch (Exception e) {
      e.printStackTrace();
      isAccountDeleted = false;
    } finally {
      if (isAccountDeleted) {
        reportLogger.log(LogStatus.PASS,
            "Account '<b style=\"color:blue;\">" + accountName + "</b>' deleted succesfully ");
      } else {
        reportLogger.log(LogStatus.FAIL, "Failed to delete Account "
            + reportLogger.addScreenCapture(getScreenshot(webDriver, "Failed to delete Account")));
      }
    }
  }
}
