package com.cigniti.salesforce.automation.scripts;

import java.util.Hashtable;

import org.testng.annotations.Test;

import com.cigniti.salesforce.automation.actions.ActionEngine;
import com.cigniti.salesforce.automation.pages.ContactsPage;
import com.cigniti.salesforce.automation.pages.HomePage;
import com.cigniti.salesforce.automation.utilities.ExcelLib;

/**
 * @author E001987
 *
 */
public class AutoScriptContactCreation extends ActionEngine{
  ExcelLib excelLib = new ExcelLib();
	HomePage homepage = new HomePage();
	ContactsPage createContactPage = new ContactsPage();
	
	/**
	 * @throws Throwable
	 */
	@Test
	public void createContact() throws Throwable {
		try {
		  Hashtable<String, String> testData = excelLib.getData("CreateContact", "RegressionSuite");
		  reportLogger = extent.startTest("Create Contact", "AutoScriptContactCreation");
			homepage.loginApplication();
			createContactPage.createContact(testData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}





