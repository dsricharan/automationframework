package com.cigniti.salesforce.automation.utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

/**
 * @author E001987
 *
 */
public class GetScreenShot {

   /**
   * @param webDriver
   * @param screenshotName
   * @return
   * @throws IOException
   */
  public static String captureScreenshot(WebDriver webDriver, String screenshotName)
      throws IOException {
    String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
    TakesScreenshot ts = (TakesScreenshot) webDriver;
    File source = ts.getScreenshotAs(OutputType.FILE);
    // after execution, you could see a folder "FailedTestsScreenshots"
    // under src folder
    String destination = System.getProperty("user.dir") + "/Screenshots/" + screenshotName
        + dateName + ".png";
    File finalDestination = new File(destination);
    FileUtils.copyFile(source, finalDestination);
    return destination;
  }
}
