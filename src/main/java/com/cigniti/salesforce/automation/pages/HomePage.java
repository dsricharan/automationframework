package com.cigniti.salesforce.automation.pages;

import org.openqa.selenium.By;

import com.cigniti.salesforce.automation.actions.ActionEngine;
import com.cigniti.salesforce.automation.utilities.ConfigManager;
import com.relevantcodes.extentreports.LogStatus;

public class HomePage extends ActionEngine {
    private By textUserName = By.id("username");
    private By textPassWord = By.id("password");
    private By buttonLogIntoSandbox = By.id("Login");
    private By textSandbox = By.xpath("//header[@id='oneHeader']//div/span[contains(text(), 'Sandbox:')]");
	
    public void launch(String url) {
        try {
            launchUrl(url);
            waitForPageToLoad();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
    
    public void login() throws Throwable {
        try {
            type(textUserName, ConfigManager.getProperty("username").trim(), "User Name");
            type(textPassWord, ConfigManager.getProperty("password").trim(), "Password");
            click(buttonLogIntoSandbox, "Login Button");
            verifyText(textSandbox, ConfigManager.getProperty("sandbox").trim());
        } catch (Exception e) {
            e.getStackTrace()[0].getLineNumber();
            reportLogger.log(LogStatus.FAIL, e.getStackTrace()[0].getLineNumber()+"");
        }
    }
    
    public void loginApplication() throws Throwable {
    	try {
    		launch(ConfigManager.getProperty("url").trim());
    		login();
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
		
	}
}