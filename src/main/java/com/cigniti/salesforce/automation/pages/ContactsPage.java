package com.cigniti.salesforce.automation.pages;

import java.util.Hashtable;
import org.openqa.selenium.By;
import com.cigniti.salesforce.automation.actions.ActionEngine;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author E001987
 *
 */
public class ContactsPage extends ActionEngine {
  
  private By linkContactsTab = By.xpath("//a[@title='Contacts']");
  private By textPageType = By.xpath("//nav[@aria-label='Breadcrumbs']");
  private By buttonNew = By.xpath("//div[@title='New']");
  private By textHeaderNewContact = By.xpath("//div[@class='actionBody']//h2");
  private By textFirstName = By.xpath("//span[text()='First Name']/parent::label/following-sibling::input");
  private By textLastName = By.xpath("//span[text()='Last Name']/parent::label/following-sibling::input");
  private By textLastNameErrorMessage = By.xpath("//span[text()='Last Name']/ancestor::div/following-sibling::ul/li");
  private By textAccountName = By.xpath("//span[text()='Account Name']/parent::label/following-sibling::div//input");
  private By textAccountNameErrorMessage = By.xpath("//span[text()='Account Name']/ancestor::div/following-sibling::ul/li");
  private By textPhone = By.xpath("//span[text()='Phone']/parent::label/following-sibling::input");
  private By textMobile = By.xpath("//span[text()='Mobile']/parent::label/following-sibling::input");
  private By textEmail = By.xpath("//span[text()='Email']/parent::label/following-sibling::input");
  private By selectDesignationLevel = By.xpath("//span[text()='Designation Level']/parent::span/following-sibling::div//a");
  private By selectBusinessUnit = By.xpath("//span[text()='BusinessUnit']/parent::span/following-sibling::div//a");
  private By textBusinessUnitErrorMessage = By.xpath("//span[text()='BusinessUnit']/ancestor::div/following-sibling::ul/li");
  private By textareaMailingStreet = By.xpath("//span[text()='Mailing Street']/parent::label/following-sibling::textarea");
  private By textMailingCity = By.xpath("//span[text()='Mailing City']/parent::label/following-sibling::input");
  private By textMailingStateOrProvince = By.xpath("//span[contains(text(),'Mailing State')]/parent::label/following-sibling::input");
  private By textMailingZipOrPostalCode = By.xpath("//span[contains(text(),'Mailing Zip')]/parent::label/following-sibling::input");
  private By textMailingCountry = By.xpath("//span[text()='Mailing Country']/parent::label/following-sibling::input");
  private By buttonSave = By.xpath("//button[@title='Save']");
  
  /**
   * @param testData
   * @throws Throwable
   */
  public void createContact(Hashtable<String, String> testData) throws Throwable{
    boolean isContactCreated = false;
    try{
      waitForElementToBeClickable(linkContactsTab, 20);
      waitForSeconds(3);
      jsClick(linkContactsTab, "Contacs Tab");
      verifyText(textPageType, testData.get("Contacts_Page_Title"));
      click(buttonNew, "New button");
      verifyText(textHeaderNewContact, testData.get("New_Contact_Header"));
      click(buttonSave, "Save button");
      scrollPageIntoView(textLastNameErrorMessage);
      verifyText(textLastNameErrorMessage, testData.get("Last_Name_Error_Message"));
      scrollPageIntoView(textAccountNameErrorMessage);
      verifyText(textAccountNameErrorMessage, testData.get("Account_Name_Error_Message"));
      scrollPageIntoView(textBusinessUnitErrorMessage);
      verifyText(textBusinessUnitErrorMessage, testData.get("Business_Unit_Error_Message"));
      type(textFirstName, testData.get("First_Name"), "First Name");
      type(textLastName, testData.get("Last_Name"), "Last Name");
      type(textPhone, testData.get("Phone"), "Phone");
      type(textMobile, testData.get("Mobile"), "Mobile");
      click(textAccountName, "Account Name");
      click(By.xpath("//div[@title='"+testData.get("Account_Name")+"']"), testData.get("Account_Name"));
      type(textEmail, testData.get("Email"), "Email");
      click(selectDesignationLevel, "Designation Level");
      click(By.xpath("//a[@title='"+testData.get("Designation_Level")+"']"), testData.get("Designation_Level"));
      click(selectBusinessUnit, "BusinessUnit");
      click(By.xpath("//a[@title='"+testData.get("Business_Unit")+"']"), testData.get("Business_Unit"));
      type(textareaMailingStreet, testData.get("Mailing_Street"), "Mailing Street");
      type(textMailingCity, testData.get("Mailing_City"), "Mailing City");
      type(textMailingStateOrProvince, testData.get("Mailing_State"), "Mailing State/Province");
      type(textMailingZipOrPostalCode, testData.get("Mailing_Zip"), "Mailing Zip/Postal Code");
      type(textMailingCountry, testData.get("Mailing_Country"), "Mailing Country");
      click(buttonSave, "Save button");
      isContactCreated = isElementPresent(By.xpath("//div/span[text()='"+testData.get("First_Name")+" "+testData.get("Last_Name")+"']"));
    }catch(Exception e){
      e.printStackTrace();
    }finally{
      if (isContactCreated) {
        reportLogger.log(LogStatus.PASS, "Contact '<b style=\"color:blue;\">"+testData.get("First_Name")+" "+testData.get("Last_Name")+"</b>' created succesfully ");
      } else {
        reportLogger.log(LogStatus.FAIL, "Failed to create contact "+ reportLogger.addScreenCapture(getScreenshot(webDriver, "Failed to create contact")));
      }
    }
  }
  
}
