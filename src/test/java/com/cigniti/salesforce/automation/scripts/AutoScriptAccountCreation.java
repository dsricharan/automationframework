package com.cigniti.salesforce.automation.scripts;

import java.util.Hashtable;
import org.testng.annotations.Test;
import com.cigniti.salesforce.automation.actions.ActionEngine;
import com.cigniti.salesforce.automation.pages.AccountsPage;
import com.cigniti.salesforce.automation.pages.HomePage;
import com.cigniti.salesforce.automation.utilities.ExcelLib;

/**
 * @author E001987
 *
 */
public class AutoScriptAccountCreation extends ActionEngine{
  ExcelLib excelLib = new ExcelLib();
  HomePage homepage = new HomePage();
  AccountsPage createAccountPage = new AccountsPage();
  
  /**
   * @throws Throwable
   */
  @Test
  public void createAccount() throws Throwable {
    try {
      Hashtable<String, String> testData = excelLib.getData("CreateAccount", "RegressionSuite");
      reportLogger = extent.startTest("Create Account", "AutoScriptAccountCreation");
      homepage.loginApplication();
      createAccountPage.createAccount(testData);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}





